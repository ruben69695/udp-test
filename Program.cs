﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace udp_test
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            var client = new DatagramClient(new IPEndPoint(IPAddress.Any, 8000));
            client.Start();

            new Thread(() => {
                while(true)
                {
                    Thread.Sleep(1000);
                }
            }).Start();
        }
    }

    public class UdpState
    {
        public byte[] data { get; set; } = new byte[8124];
        public int GetBufferSize() => 8124;
        public Socket Source { get; set; }
        public IPEndPoint endPoint { get; set;}
    }

    public class DatagramClient
    {
        private Socket _receiveSocket;
        private IPEndPoint _endPoint;
        public DatagramClient(IPEndPoint endpoint)
        {
            _receiveSocket = new Socket(endpoint.AddressFamily, SocketType.Dgram, ProtocolType.Udp);
            _endPoint = endpoint;
        }

        public void Start()
        {
            var udpState = new UdpState() {
                Source = _receiveSocket,
                endPoint = _endPoint
            };

            var endpoint = (EndPoint)_endPoint;
            _receiveSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            _receiveSocket.Bind(_endPoint);
            _receiveSocket.BeginReceiveFrom(udpState.data, 0, udpState.GetBufferSize(), 0, ref endpoint, new AsyncCallback(ReceiveCallback), udpState);
        }

        private void ReceiveCallback(IAsyncResult ar)
        {
            var udpState = (UdpState)ar.AsyncState;
            var endpoint = (EndPoint)udpState.endPoint;
            var readBytes = udpState.Source.EndReceive(ar);
            string data = Encoding.ASCII.GetString(udpState.data, 0, readBytes);

            System.Console.WriteLine($"Recibido: {data}");
            _receiveSocket.BeginReceiveFrom(udpState.data, 0, udpState.GetBufferSize(), 0, ref endpoint, new AsyncCallback(ReceiveCallback), udpState);
        }        
    }
}
